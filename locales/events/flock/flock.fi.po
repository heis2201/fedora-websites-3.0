#. extracted from content/events/flock.yml
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-26 13:33-0700\n"
"PO-Revision-Date: 2022-12-15 03:20+0000\n"
"Last-Translator: Jan Kuparinen <copper_fin@hotmail.com>\n"
"Language-Team: Finnish <https://translate.fedoraproject.org/projects/"
"fedora-websites-3-0/flock-to-fedora/fi/>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Translate Toolkit 3.7.3\n"

#: path
msgid "flock"
msgstr "flock"

#: title
msgid "The Fedora Project Conference"
msgstr "Fedora-projektin konferenssi"

#: description
msgid "August 5 - 7, 2023\n"
msgstr "5.–7. elokuuta 2023\n"

#: header_images-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/flock_logo_barcelona.png"
msgstr ""

#: header_images-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/flock_background.jpg"
msgstr ""

#: links-%3E[0]-%3Etext
msgid "Register"
msgstr "Rekisteröidy"

#: links-%3E[0]-%3Eurl
#, read-only
msgctxt "links->[0]->url"
"links->[0]->url"
"links->[0]->url"
"links->[0]->url"
msgid "#"
msgstr ""

#: links-%3E[1]-%3Etext
msgid "Calendar"
msgstr "Kalenteri"

#: links-%3E[1]-%3Eurl
#, read-only
msgctxt "links->[1]->url"
msgid "#"
msgstr ""

#: sections-%3E[0]-%3EsectionTitle
msgid "Events"
msgstr "Tapahtumat"

#: sections-%3E[0]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/flock_nest_logo.png"
msgstr ""

#: sections-%3E[0]-%3Econtent-%3E[0]-%3Etitle
msgid "Virtual"
msgstr "Virtuaalinen"

#: sections-%3E[0]-%3Econtent-%3E[0]-%3Edescription
msgid "A 3 day conference hosted on Hopin.io"
msgstr "Hopin.io-sivustolla isännöity 3 päivän konferenssi"

#: sections-%3E[0]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/flock_hatch_logo.png"
msgstr ""

#: sections-%3E[0]-%3Econtent-%3E[1]-%3Etitle
msgid "Community"
msgstr "Yhteisö"

#: sections-%3E[0]-%3Econtent-%3E[1]-%3Edescription
msgid "Community events hosted around the world"
msgstr "Yhteisötapahtumia järjestetään ympäri maailmaa"

#: sections-%3E[1]-%3EsectionTitle
msgid "Explore the latest in Open Source"
msgstr "Tutustu avoimen lähdekoodin uusimpiin tapahtumiin"

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Etitle
msgid "Workshops"
msgstr "Työpajat"

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"Workshops and hackfests offer opportunities for active learning and "
"collaboration."
msgstr ""
"Työpajat ja hackfestit tarjoavat mahdollisuuksia aktiiviseen oppimiseen ja "
"yhteistyöhön."

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/flock_workshops_logo.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Elink-%3Etext
msgid "View Projects"
msgstr "Näytä projektit"

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Elink-%3Eurl
#, read-only
msgctxt "sections->[1]->content->[0]->link->url"
msgid "#"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Etitle
msgid "Sessions"
msgstr "Istunnot"

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/flock_sessions_logo.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"Presentations that showcase the work done in Fedora and planning for the "
"future."
msgstr ""
"Esityksiä, jotka esittelevät Fedorassa tehtyä työtä ja tulevaisuuden "
"suunnittelua."

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Elink-%3Etext
msgid "View Topics"
msgstr "Näytä aiheet"

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Elink-%3Eurl
#, read-only
msgctxt "sections->[1]->content->[1]->link->url"
msgid "#"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Etitle
msgid "Social Events"
msgstr "Sosiaaliset tapahtumat"

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Eimage
#, read-only
msgid "public/assets/images/flock_socialevents_logo.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"Casual, facilitated, and interactive events for meeting and socializing with "
"Fedorans."
msgstr ""
"Rento, avustettu ja interaktiivinen tapahtuma fedoralaisten tapaamiseen ja "
"seurustelua varten."

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Elink-%3Etext
msgid "View Socials"
msgstr "Näytä yhteistilaisuudet"

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Elink-%3Eurl
#, read-only
msgctxt "sections->[1]->content->[2]->link->url"
msgid "#"
msgstr ""

#: sections-%3E[2]-%3EsectionTitle
msgid "Watch Footage from Past Events"
msgstr "Katso videomateriaalia menneistä tapahtumista"

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Etitle
msgid "What to expect"
msgstr "Mitä odottaa"

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "https://www.youtube.com/embed/LqBVHz76Wxc"
msgstr ""

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"Flock is an annual conference for contributors of Fedora Linux. It is where "
"the community plans and showcases the strategy and work on the project. "
"Check out the recordings from previous years of Flock and Nest."
msgstr ""
"Flock on vuotuinen konferenssi Fedora Linuxin edistäjille. Siellä yhteisö "
"suunnittelee ja esittelee strategiaa ja työtä projektin parissa. Katso "
"tallenteet Flockin ja Nestin aiemmilta vuosilta."

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Elink-%3Etext
msgid "Visit Fedora Youtube"
msgstr "Vieraile Fedora Youtubessa"

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Elink-%3Eurl
#, read-only
msgid "https://www.youtube.com/fedoraproject"
msgstr ""

#: sections-%3E[3]-%3EsectionTitle
msgid "A Hybrid Experience"
msgstr "Hybridikokemus"

#: sections-%3E[3]-%3EsectionDescription
msgid ""
"The location of Flock changes each year between Europe and North America. In "
"2020 Fedorans went virtual with Nest, now we offer a hybrid version of Flock "
"to make it more accessible to our community."
msgstr ""
"Flockin sijainti vaihtuu vuosittain Euroopan ja Pohjois-Amerikan välillä. "
"Vuonna 2020 Fedora siirtyi virtuaaliseksi Nestin kanssa, nyt tarjoamme "
"Flockin hybridiversion, jotta se olisi helpommin saatavilla yhteisöllemme."

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Etitle
msgctxt "sections->[3]->content->[0]->title"
msgid "July 1 - 31 2023"
msgstr "1.–31.7.2023"

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/flock_logo_mini.png"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"Flock offers an immersive in person experience. Attend sessions, collaborate "
"with others, explore and meet new people. Plus, Flock is free to attend!"
msgstr ""
"Flock tarjoaa mukaansatempaavan henkilökohtaisen kokemuksen. Osallistu "
"istuntoihin, tee yhteistyötä muiden kanssa, tutustu ja tapaa uusia ihmisiä. "
"Lisäksi Flock on ilmainen!"

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Etitle
msgctxt "sections->[3]->content->[1]->title"
msgid "July 1 - 31 2023"
msgstr "1.–31.7.2023"

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/nest-dual-colour-navy.png"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"During the Covid-19 pandemic, we created Nest as a virtual alternative to "
"Flock. Now it exists as a virtual option for those who cannot attend in "
"person. Making Flock accessible for everyone!"
msgstr ""
"Covid-19-pandemian aikana loimme Nestin virtuaaliseksi vaihtoehdoksi "
"Flockille. Nyt se on olemassa virtuaalisena vaihtoehtona niille, jotka eivät "
"voi osallistua henkilökohtaisesti, avaten Flockin kaikkien ulottuville!"

#: sections-%3E[4]-%3EsectionTitle
msgid "Interested in hosting your own event?"
msgstr "Kiinnostaako oman tapahtumasi järjestäminen?"

#: sections-%3E[4]-%3EsectionDescription
msgid ""
"July 1 - 31 2023\n"
"In the month leading up to Flock, Fedorans are encouraged to organize local "
"community events. Set the format and topic of your choice. The Fedora "
"Council will help support your initiative with financial and organizating "
"aid. Click below to set up a Hatch! Deadline for submissions is May 29."
msgstr ""
"1.–31.7.2023\n"
"Flockia edeltävänä kuukautena fedoralaisia kannustetaan järjestämään "
"paikallisia yhteisötapahtumia. Aseta haluamasi tapa ja aihe. Fedora Council "
"auttaa tukemaan aloitettasi taloudellisella ja organisatorisella tuella. "
"Napsauta alapuolelta ja aseta luukku! Hakuaika päättyy 29. toukokuuta."

#: sections-%3E[5]-%3EsectionTitle
msgid "We are Fedora"
msgstr "Olemme Fedora"

#: sections-%3E[5]-%3Eimage
#, read-only
msgid "public/assets/images/iot_flock_background.jpg"
msgstr ""

#: sections-%3E[5]-%3EsectionDescription
msgid ""
"The Fedora Project envisions a world where everyone benefits from free and "
"open source software built by inclusive, welcoming, and open-minded "
"communities. Stay connected during Flock through the following channels."
msgstr ""
"Fedora-projekti visioi maailmasta, jossa kaikki hyötyvät ilmaisista ja "
"avoimen lähdekoodin ohjelmistoista, jotka osallistavat, vieraanvaraiset ja "
"ennakkoluulottomat yhteisöt ovat rakentaneet. Pysy yhteydessä Flockin aikana "
"seuraavien kanavien kautta."

#: sections-%3E[5]-%3Econtent-%3E[0]-%3Etitle
msgid "Discussions"
msgstr "Keskustelut"

#: sections-%3E[5]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/discussions_icon.png"
msgstr ""

#: sections-%3E[5]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"Use our messaging board Fedora Discussions to stay up to date on Flock "
"Notifications. <b><a href=\"https://discussion.fedoraproject.org/\">Check is "
"out here</a></b>."
msgstr ""
"Käytä viestialustaamme Fedora-keskusteluja pysyäksesi ajan tasalla Flock-"
"ilmoituksista. <b><a href=\"https://discussion.fedoraproject.org/\">Teksti "
"on täällä</a></b>."

#: sections-%3E[5]-%3Econtent-%3E[1]-%3Etitle
msgid "Twitter"
msgstr "Twitter"

#: sections-%3E[5]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/twitter_icon.png"
msgstr ""

#: sections-%3E[5]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"Use the <b>#NestWithFedora</b> hashtags on your preferred social media "
"platforms. Follow <b><a href=\"https://twitter.com/fedora\">@fedora</a></b> "
"on Twitter for updates."
msgstr ""
"Käytä <b>#NestWithFedora</b>-hashtagia haluamillasi sosiaalisen median "
"alustoilla. Seuraa <b><a href=\"https://twitter.com/fedora\">@fedora</a></b> "
"Twitterissä saadaksesi päivityksiä."

#: sections-%3E[5]-%3Econtent-%3E[2]-%3Etitle
msgid "Matrix & IRC"
msgstr "Matrix & IRC"

#: sections-%3E[5]-%3Econtent-%3E[2]-%3Eimage
#, read-only
msgid "public/assets/images/matrix_icon.png"
msgstr ""

#: sections-%3E[5]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"Our main IRC channel for the event is <b>#fedora-flock</b> on <b>libera."
"chat</b>. Join <b><a href=\"https://web.libera.chat/?channels=#fedora-flock\""
">#fedora-flock</a></b> to chat on Matrix or IRC."
msgstr ""
"Tapahtuman pää IRC-kanavamme on <b>#fedora-flock</b> osoitteessa <b>libera."
"chat</b>. Liity <b><a href=\"https://web.libera.chat/?channels=#fedora-"
"flock\">#fedora-flockiin</a></b> keskustellaksesi Matrixissa tai IRC:ssä."

#: sections-%3E[5]-%3Econtent-%3E[3]-%3Etitle
msgid "Ask Fedora"
msgstr "Kysy Fedorasta"

#: sections-%3E[5]-%3Econtent-%3E[3]-%3Eimage
#, read-only
msgid "public/assets/images/ask_fedora_icon.png"
msgstr ""

#: sections-%3E[5]-%3Econtent-%3E[3]-%3Edescription
msgid ""
"New to Fedora? Check our <b><a href=\"https://ask.fedoraproject.org/\""
">community forum</a></b> for support and help with frequently asked "
"questions."
msgstr ""
"Uusi Fedorassa? Katso <b><a href=\"https://ask.fedoraproject.org/\">yhteisön "
"keskusteluryhmästämme</a></b> tukea ja apua usein kysyttyihin kysymyksiin."

#: sections-%3E[6]-%3EsectionTitle
msgid "Our Sponsors"
msgstr "Sponsorimme"

#: sections-%3E[6]-%3EsectionDescription
msgid ""
"Thank you to our sponsors, supporting this event is one of the ways that "
"they contribute to open source."
msgstr ""
"Kiitos sponsoreillemme, tämän tapahtuman tukeminen on yksi tavoista, joilla "
"he osallistuvat avoimen lähdekoodin projekteihin."

#: sections-%3E[6]-%3Econtent-%3E[0]-%3Ename
msgid "Red Hat"
msgstr "Red Hat"

#: sections-%3E[6]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/sponsors/Red_Hat_logo.png"
msgstr ""

#: sections-%3E[6]-%3Econtent-%3E[0]-%3Edescription
msgid "platinium"
msgstr "platina"

#: sections-%3E[6]-%3Econtent-%3E[1]-%3Ename
msgid "Lenovo"
msgstr "Lenovo"

#: sections-%3E[6]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/sponsors/Lenovo_logo.png"
msgstr ""

#: sections-%3E[6]-%3Econtent-%3E[1]-%3Edescription
msgctxt "sections->[6]->content->[1]->description"
msgid "gold"
msgstr "kulta"

#: sections-%3E[6]-%3Econtent-%3E[2]-%3Ename
msgid "AlmaLinux"
msgstr "AlmaLinux"

#: sections-%3E[6]-%3Econtent-%3E[2]-%3Eimage
#, read-only
msgid "public/assets/images/sponsors/AlmaLinux_logo.png"
msgstr ""

#: sections-%3E[6]-%3Econtent-%3E[2]-%3Edescription
msgctxt "sections->[6]->content->[2]->description"
msgid "gold"
msgstr "kulta"

#: sections-%3E[6]-%3Econtent-%3E[3]-%3Ename
msgid "openSuse"
msgstr "openSuse"

#: sections-%3E[6]-%3Econtent-%3E[3]-%3Eimage
#, read-only
msgid "public/assets/images/sponsors/opensuse_logo.png"
msgstr ""

#: sections-%3E[6]-%3Econtent-%3E[3]-%3Edescription
msgctxt "sections->[6]->content->[3]->description"
msgid "silver"
msgstr "hopea"

#: sections-%3E[6]-%3Econtent-%3E[4]-%3Ename
msgid "some Other Sponsor"
msgstr "muita sponsoreita"

#: sections-%3E[6]-%3Econtent-%3E[4]-%3Eimage
#, read-only
msgctxt "sections->[6]->content->[4]->image"
msgid ""
msgstr ""

#: sections-%3E[6]-%3Econtent-%3E[4]-%3Edescription
msgctxt "sections->[6]->content->[4]->description"
msgid "silver"
msgstr "hopea"

#: sections-%3E[6]-%3Econtent-%3E[5]-%3Ename
msgid "another Cool Sponsor"
msgstr "toinen hieno sponsori"

#: sections-%3E[6]-%3Econtent-%3E[5]-%3Eimage
#, read-only
msgctxt "sections->[6]->content->[5]->image"
msgid ""
msgstr ""

#: sections-%3E[6]-%3Econtent-%3E[5]-%3Edescription
msgctxt "sections->[6]->content->[5]->description"
"sections->[6]->content->[5]->description"
"sections->[6]->content->[5]->description"
msgid "bronze"
msgstr "pronssi"

#: sections-%3E[6]-%3Econtent-%3E[6]-%3Ename
msgid "GitLab"
msgstr "GitLab"

#: sections-%3E[6]-%3Econtent-%3E[6]-%3Eimage
#, read-only
msgid "public/assets/images/sponsors/Gitlab_logo.png"
msgstr ""

#: sections-%3E[6]-%3Econtent-%3E[6]-%3Edescription
msgctxt "sections->[6]->content->[6]->description"
msgid "bronze"
msgstr "pronssi"

#: sections-%3E[6]-%3Econtent-%3E[7]-%3Ename
msgid "datto"
msgstr "datto"

#: sections-%3E[6]-%3Econtent-%3E[7]-%3Eimage
#, read-only
msgid "public/assets/images/sponsors/datto_logo.png"
msgstr ""

#: sections-%3E[6]-%3Econtent-%3E[7]-%3Edescription
msgctxt "sections->[6]->content->[7]->description"
msgid "bronze"
msgstr "pronssi"

#: sections-%3E[6]-%3Econtent-%3E[8]-%3Ename
msgid "daskeyboard"
msgstr "das-näppäimistö"

#: sections-%3E[6]-%3Econtent-%3E[8]-%3Eimage
#, read-only
msgid "public/assets/images/sponsors/daskeyboard_logo.png"
msgstr ""

#: sections-%3E[6]-%3Econtent-%3E[8]-%3Edescription
msgctxt "sections->[6]->content->[8]->description"
msgid "bronze"
msgstr "pronssi"

#: sections-%3E[6]-%3Econtent-%3E[9]-%3Ename
msgid "Gnome"
msgstr "Gnome"

#: sections-%3E[6]-%3Econtent-%3E[9]-%3Eimage
#, read-only
msgid "public/assets/images/sponsors/Gnome_logo.png"
msgstr ""

#: sections-%3E[6]-%3Econtent-%3E[9]-%3Edescription
msgctxt "sections->[6]->content->[9]->description"
"sections->[6]->content->[9]->description"
"sections->[6]->content->[9]->description"
msgid "media"
msgstr "media"

#: sections-%3E[6]-%3Econtent-%3E[10]-%3Ename
msgid "TuxDigital"
msgstr "TuxDigital"

#: sections-%3E[6]-%3Econtent-%3E[10]-%3Eimage
#, read-only
msgid "public/assets/images/sponsors/TuxDigital_logo.png"
msgstr ""

#: sections-%3E[6]-%3Econtent-%3E[10]-%3Edescription
msgctxt "sections->[6]->content->[10]->description"
msgid "media"
msgstr "media"

#: sections-%3E[6]-%3Econtent-%3E[11]-%3Ename
msgid "KDE"
msgstr "KDE"

#: sections-%3E[6]-%3Econtent-%3E[11]-%3Eimage
#, read-only
msgid "public/assets/images/sponsors/KDE_logo.png"
msgstr ""

#: sections-%3E[6]-%3Econtent-%3E[11]-%3Edescription
msgctxt "sections->[6]->content->[11]->description"
msgid "media"
msgstr "media"

#: sections-%3E[6]-%3Econtent-%3E[12]-%3Ename
msgid "opensource.com"
msgstr "opensource.com"

#: sections-%3E[6]-%3Econtent-%3E[12]-%3Eimage
#, read-only
msgid "public/assets/images/sponsors/opensource.com_logo.png"
msgstr ""

#: sections-%3E[6]-%3Econtent-%3E[12]-%3Edescription
msgctxt "sections->[6]->content->[12]->description"
msgid "media"
msgstr "media"

#: sections-%3E[6]-%3Econtent-%3E[13]-%3Ename
msgid "none"
msgstr "ei mitään"

#: sections-%3E[6]-%3Econtent-%3E[13]-%3Edescription
msgid "placeholder"
msgstr "paikanpitäjä"

#: sections-%3E[7]-%3EsectionTitle
msgid "Benefits of Sponsoring Flock"
msgstr "Flockin sponsoroinnin hyödyt"

#: sections-%3E[7]-%3Econtent-%3E[0]-%3Etitle
msgid "Brand Exposure"
msgstr "Brändin tunnettavuus"

#: sections-%3E[7]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"Sponsors are advertised in several places at Flock. We also post about "
"sponsors on social media. Your brand will be distributed globally in our "
"swag bag."
msgstr ""
"Sponsoreja mainostetaan useissa paikoissa Flockissa. Julkaisemme tietoa "
"sponsoreista myös sosiaalisessa mediassa. Brändiäsi jaetaan "
"maailmanlaajuisesti swag bag -pussissamme."

#: sections-%3E[7]-%3Econtent-%3E[1]-%3Etitle
msgid "Spotlight"
msgstr "Valokeila"

#: sections-%3E[7]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"Our tierd sponsorship levels will all give you great visibility at Flock. "
"Your booth will be showcased on the Expo page to maximize engagement."
msgstr ""
"Sponsorointitasomme antavat sinulle loistavan näkyvyyden Flockissa. "
"Näyttelyosastosi esitellään Expo-sivulla osallistumisen maksimoimiseksi."

#: sections-%3E[7]-%3Econtent-%3E[2]-%3Etitle
msgid "Networking"
msgstr "Verkostoituminen"

#: sections-%3E[7]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"Flock attracts hundreds of amazing open source community members who work on "
"and use software daily. Engage with our vibrant community in sessions and "
"social hours."
msgstr ""
"Flock houkuttelee satoja uskomattomia avoimen lähdekoodin yhteisön jäseniä, "
"jotka työskentelevät ja käyttävät ohjelmistoja päivittäin. Ota yhteyttä "
"eloisaan yhteisöömme istunnoissa ja sosiaalisten tuntien aikana."

#: sections-%3E[7]-%3Econtent-%3E[3]-%3Etitle
msgid "Booths"
msgstr "Näyttelytilat"

#: sections-%3E[7]-%3Econtent-%3E[3]-%3Edescription
msgid ""
"All sponsor booths include a video, customizable experience, chat, and "
"social media engagement. Click below to learn more."
msgstr ""
"Kaikki sponsorikopit sisältävät videon, mukautettavan kokemuksen, chatin ja "
"sosiaalisen median käyttämisen. Napsauta alapuolelta saadaksesi lisätietoja."

#~ msgctxt "sections->[3]->sectionDescription"
#~ msgid ""
#~ "The location of Flock changes each year between Europe and North America. In "
#~ "2020 Fedorans went virtual with Nest, now we offer a hybrid version of Flock "
#~ "to make it more accessible to our community."
#~ msgstr ""
#~ "Flockin sijainti vaihtuu vuosittain Euroopan ja Pohjois-Amerikan välillä. "
#~ "Vuonna 2020 Fedora siirtyi virtuaaliseksi Nestin kanssa, nyt tarjoamme "
#~ "Flockin hybridiversion, jotta se olisi helpommin saatavilla yhteisöllemme."

#~ msgctxt "sections->[3]->content->[0]->description"
#~ msgid ""
#~ "The location of Flock changes each year between Europe and North America. In "
#~ "2020 Fedorans went virtual with Nest, now we offer a hybrid version of Flock "
#~ "to make it more accessible to our community."
#~ msgstr ""
#~ "Flockin sijainti vaihtuu vuosittain Euroopan ja Pohjois-Amerikan välillä. "
#~ "Vuonna 2020 Fedora siirtyi virtuaaliseksi Nestin kanssa, nyt tarjoamme "
#~ "Flockin hybridiversion, jotta se olisi helpommin saatavilla yhteisöllemme."

#~ msgid "Flock To Fedora"
#~ msgstr "Flock Fedoraan"

#~ msgid "The Fedora Project Conference, August 5 - 7, 2023\n"
#~ msgstr "Fedora-projektikonferenssi, 5.-7.8.2023\n"
