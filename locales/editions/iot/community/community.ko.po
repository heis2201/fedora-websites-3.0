#. extracted from content/editions/iot/community.yml
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-26 13:33-0700\n"
"PO-Revision-Date: 2022-11-18 13:47+0000\n"
"Last-Translator: 김인수 <simmon@nplob.com>\n"
"Language-Team: Korean <https://translate.fedoraproject.org/projects/"
"fedora-websites-3-0/editionsiotcommunity/ko/>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Translate Toolkit 3.7.3\n"

#: path
msgid "community"
msgstr "커뮤니티"

#: title
msgid "Community"
msgstr "커뮤니티"

#: description
msgid "Join a community of users and contributors focused on Fedora IoT."
msgstr "페도라 사물인터넷(IoT)에 중점을 둔 사용자와 공헌자의 커뮤니티에 참여합니다."

#: sections-%3E[0]-%3EsectionTitle
msgid "Header Second Half"
msgstr "헤더 후반"

#: sections-%3E[0]-%3EsectionDescription
msgid ""
"Fedora IoT is created by a team in the Fedora Project community called the "
"IoT Working Group. It is comprised of official members who have decision-"
"making powers, as well as other contributors. Learn more about this group "
"and how you can get involved on the IoT Working Group website."
msgstr ""
"페도라 사물인터넷(IoT)는 IoT 작업 그룹이라고 불리는 페도라 프로젝트 "
"커뮤니티에 있는 팀에 의해 생성되었습니다. 의사-결정 권한을 가진 공식 구성원 "
"뿐만 아니라 다른 공헌자로 구성됩니다. IoT 작업 그룹 웹주소에서 이 그룹과 "
"참여 방법에 대해 더 알아보세요."

#: sections-%3E[1]-%3EsectionTitle
msgid "Communication Channels"
msgstr "통신 채널"

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Etitle
msgid "Forum"
msgstr "포럼"

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"The **Fedora IoT** tag on Fedora's Discourse-based discussion forum is a "
"great place to connect with other Fedora Workstation users and contributors."
msgstr ""
"페도라 디스코드-기반의 토의 포럼에서 **페도라 사물인터넷(IoT)** 표시는 다른 "
"페도라 웍스테이션 사용자와 공헌자를 함께 연결하는 훌륭한 장소입니다."

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/fedora-discussion-plus-icon.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Elink-%3Etext
msgctxt "sections->[1]->content->[0]->link->text"
msgid "Visit Now"
msgstr "지금 방문하세요"

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Elink-%3Eurl
#, read-only
msgid "https://discussion.fedoraproject.org/tag/iot"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Etitle
msgid "Chat"
msgstr "대화"

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"You can chat with the Fedora IoT Working Group via either Matrix or IRC: "
"#iot:fedoraproject.org on Matrix and #fedora-iot on irc.libera.chat."
msgstr ""
"당신은 매트릭스와 #fedora-iot on irc.libera.chat에서 매트릭스 또는 IRC: #iot:"
"fedoraproject.org를 통해서 페도라 사물인터넷(IoT) 작업 그룹과 대화 할 수 "
"있습니다."

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/fedora-chat-plus-element.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Elink-%3Etext
msgid "Chat Now"
msgstr "지금 대화하세요"

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Elink-%3Eurl
#, read-only
msgid "https://chat.fedoraproject.org/#/welcome"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Etitle
msgid "Mailing List"
msgstr "전자우편 목록"

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"The Fedora IoT Working Group sends meeting agendas and minutes and has "
"discussions on their mailing list at iot@lists.fedoraproject.org."
msgstr ""
"페도라 사물인터넷(IoT) 작업 그룹은 iot@lists.fedoraproject.org에 있는 이들 "
"전자우편 목록에서 회의 의제와 의사록을 보내고 토론합니다."

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Elink-%3Etext
msgctxt "sections->[1]->content->[2]->link->text"
msgid "Visit Now"
msgstr "지금 방문하세요"

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Elink-%3Eurl
#, read-only
msgid ""
"https://lists.fedoraproject.org/archives/list/iot@lists.fedoraproject.org/"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Eimage
#, read-only
msgid "public/assets/images/hyperkitty-plus-logo.png"
msgstr ""

#: sections-%3E[2]-%3EsectionTitle
msgid "Ways to get involved"
msgstr "참여하는 방법"

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Etitle
msgid "Report & discuss issues"
msgstr "사건 보고 & 토의"

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"You can view, file, and discuss Fedora IoT issues on the [Fedora IoT issue "
"tracker](https://pagure.io/fedora-iot/issues)."
msgstr ""
"당신은 [페도라 IoT 사건 추적기](https://pagure.io/fedora-iot/issues)에서 "
"페도라 사물인터넷(IoT) 문제를 보고, 철하며, 토의 할 수 있습니다."

#: sections-%3E[2]-%3Econtent-%3E[1]-%3Etitle
msgid "Attend a meeting"
msgstr "회의 참석"

#: sections-%3E[2]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"IoT Working Group meetings are open to all current and potential "
"contributors on Wednesdays, 11 AM US Eastern at fedora-meeting@irc.libera."
"chat."
msgstr ""
"사물인터넷(IoT) 작업 그룹 회의는 수요일 fedora-meeting@irc.libera.chat(미국 "
"동부시간 오전 11시)에 현재 및 잠재적인 공헌자 모두에게 열려있습니다."

#~ msgid "IOT Community"
#~ msgstr "IOT 커뮤니티"
